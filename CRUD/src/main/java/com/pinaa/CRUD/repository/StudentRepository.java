/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.repository;

import com.pinaa.CRUD.entity.Student;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author pinaa
 */

public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByNama(String nama);
    
    @Query("SELECT s from Student s WHERE " +
            " s.nama LIKE CONCAT('%', :query, '%')")
    List<Student> searchStudent(String query);

    Page<Student> findByNamaContainingIgnoreCase(String keyword, Pageable pageable);
}

