/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.service;

import com.pinaa.CRUD.dto.StudentDto;
import com.pinaa.CRUD.entity.Student;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author pinaa
 */

public interface StudentService {
    
    List<StudentDto> ambilDaftarStudent();
    void perbaruiDataStudent(StudentDto studentDto);
    void hapusDataStudent(Long studentNim);
    void simpanDataStudent(StudentDto studentDto);
    StudentDto cariStudentByNim(Long nim); 
    public Page<Student> findByNamaContainingIgnoreCase(String keyword, Pageable pageable);
    public Page<Student> findAll(Pageable pageable);

}
