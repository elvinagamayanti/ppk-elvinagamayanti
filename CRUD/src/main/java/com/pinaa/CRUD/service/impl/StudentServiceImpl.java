/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.service.impl;

import com.pinaa.CRUD.dto.StudentDto;
import com.pinaa.CRUD.entity.Student;
import com.pinaa.CRUD.mapper.StudentMapper;
import com.pinaa.CRUD.repository.StudentRepository;
import com.pinaa.CRUD.service.StudentService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 *
 * @author pinaa
 */

@Service
public class StudentServiceImpl implements StudentService {    
    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    
    @Override
    public List<StudentDto> ambilDaftarStudent() {
        List<Student> students = this.studentRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi map()
        List<StudentDto> studentDtos = students.stream()
                .map((student) -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());        
        return studentDtos;
    }
    
    @Override
    public void hapusDataStudent(Long studentNim) {
        studentRepository.deleteById(studentNim);        
    }
    
    @Override
    public void perbaruiDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        System.out.println(studentDto);
        studentRepository.save(student);
    }
    
    @Override
    public void simpanDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        //System.out.println(student);
        studentRepository.save(student);
    }

    @Override
    public StudentDto cariStudentByNim(Long nim) {
        Student std = studentRepository.findById(nim).get();
        return  StudentMapper.mapToStudentDto(std);
    }
    
    // Pagination
    @Override
    public Page<Student> findByNamaContainingIgnoreCase(String keyword, Pageable pageable){
        return studentRepository.findByNamaContainingIgnoreCase(keyword, pageable);
    }
    @Override
    public Page<Student> findAll(Pageable pageable){
        return studentRepository.findAll(pageable);
    }

}
