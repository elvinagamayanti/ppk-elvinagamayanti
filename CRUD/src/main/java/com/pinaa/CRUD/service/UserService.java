/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.service;

import com.pinaa.CRUD.dto.UserDto;
import com.pinaa.CRUD.entity.User;
import java.util.List;

/**
 *
 * @author pinaa
 */

public interface UserService {
    void saveUser(UserDto userDto);

    User findUserByEmail(String email);

    List<UserDto> findAllUsers();
}

