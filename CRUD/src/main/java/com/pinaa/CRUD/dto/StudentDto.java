/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.dto;

import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author pinaa
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    private Long nim;
    
    @NotEmpty(message = "Nama should not be empty")
    private String nama;
    
    @NotNull(message = "Jurusan should not be empty")
    private String jurusan;
    
    @NotNull(message = "Tanggal Lahir should not be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date tanggalLahir; 
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn;        
}
