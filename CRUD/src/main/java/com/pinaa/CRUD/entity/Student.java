/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.entity;

import jakarta.persistence.*; 
import java.util.ArrayList;
// hibernate 
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.util.Date;
import java.util.List;
import lombok.Setter;
import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author pinaa
 * Class Entity Student mewakili tabel Student di database.
 * GetSet Methods,Constructor dengan parameter semua field, dan default Constructor
 * otomatis ditambahkan saat compile time berlangsung menggunakan library lombok. 
 * 
 * Data Access Layer
 * 
 */

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_student")
public class Student {
    @Id 
    private Long nim;
    
    @Column(nullable = false)
    private String nama;
    
    @Column(name = "jurusan", nullable = true)
    private String jurusan;
    
    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date tanggalLahir; 
    
    @Column(nullable = false)
    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    
    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn;    
}

