/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pinaa.CRUD.mapper;

import com.pinaa.CRUD.dto.StudentDto;
import com.pinaa.CRUD.entity.Student;

/**
 *
 * @author pinaa
 */
public class StudentMapper {    
    // map Student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student) {
        // Membuat dto dengan builder pattern (inject dari lombok)
        StudentDto studentDto = StudentDto.builder()
                .nim(student.getNim())
                .nama(student.getNama())
                .jurusan(student.getJurusan())
                .tanggalLahir(student.getTanggalLahir())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();        
        return studentDto;
    }    
    // map Student Dto ke Student Entity
    public static Student mapToStudent(StudentDto studentDto) {
        Student student = Student.builder()
                .nim(studentDto.getNim())
                .nama(studentDto.getNama())
                .jurusan(studentDto.getJurusan())
                .tanggalLahir(studentDto.getTanggalLahir())
                .createdOn(studentDto.getCreatedOn())
                .updatedOn(studentDto.getUpdatedOn())
                .build();        
        return student;
    }
}

